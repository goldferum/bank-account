package main

import (
	"bank-account/account"
	"encoding/json"

	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var a *account.Account

type response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type responseBalance struct {
	Amount int64 `json:"amount"`
}

type responseInitAccount struct {
	Amount *int64 `json:"initialAmount"`
}

type responseAmount struct {
	Amount *int64 `json:"amount"`
}

//CreateNewAccount - create new account
func CreateNewAccount(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	decoder := json.NewDecoder(r.Body)
	var p responseInitAccount
	err := decoder.Decode(&p)
	if err != nil || p.Amount == nil {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Invalid parameters. Account is not created.")
		w.Write(resp)
		return
	}
	a = account.Open(*p.Amount)
	if a == nil {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Amount less than zero. Account is not created.")
		w.Write(resp)
		return
	}

	w.WriteHeader(200)
	resp := constructResponse("Success", "Account successfully created.")
	w.Write(resp)
}

//EditBalanceAccount - Edit balance on account
func EditBalanceAccount(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	decoder := json.NewDecoder(r.Body)
	var p responseAmount
	err := decoder.Decode(&p)

	if err != nil || p.Amount == nil {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Invalid parameters.")
		w.Write(resp)
		return
	}

	_, st := a.Balance()
	if !st {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Accoun is closed.")
		w.Write(resp)
		return
	}

	_, ok := a.Deposit(*p.Amount)
	if !ok {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Not enough money.")
		w.Write(resp)
		return
	}

	w.WriteHeader(200)
}

//GetBalance - Get balance
func GetBalance(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	balance, st := a.Balance()
	if !st {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Accoun is closed.")
		w.Write(resp)
		return
	}
	w.WriteHeader(200)
	resp := responseBalance{Amount: balance}
	respByte := MarshalJSON(resp)
	w.Write(respByte)
}

//CloseAccount - Close account
func CloseAccount(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	_, st := a.Close()

	if !st {
		w.WriteHeader(400)
		resp := constructResponse("Error", "Accoun is closed.")
		w.Write(resp)
		return
	}
	w.WriteHeader(200)

	resp := constructResponse("Success", "Account successfully closed.")
	w.Write(resp)
}

//MarshalJSON - marshal JSON
func MarshalJSON(data interface{}) []byte {
	b, err := json.Marshal(data)
	if err != nil {
		log.Println("Error marshal data:", err)
		return nil
	}
	return b
}

//constructResponse - create response
func constructResponse(status string, message string) []byte {
	resp := response{}
	resp.Status = status
	resp.Message = message
	bresp := MarshalJSON(resp)
	return bresp
}

func main() {
	router := httprouter.New()
	router.PUT("/account", CreateNewAccount)
	router.POST("/account", EditBalanceAccount)
	router.GET("/account", GetBalance)
	router.DELETE("/account", CloseAccount)

	log.Fatal(http.ListenAndServe(":8081", router))
}
