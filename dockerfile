FROM golang:1.10.2

WORKDIR /go/src/bank-account
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["bank-account"]