package account

import (
	"sync"
)

type Account struct {
	sync.Mutex
	balance int64
	status  bool
}

func Open(initialDeposit int64) *Account {
	if initialDeposit < 0 {
		return nil
	}
	return &Account{balance: initialDeposit, status: true}
}

func (a *Account) Deposit(amount int64) (newBalance int64, ok bool) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	if !a.status {
		return 0, false
	}
	if amount < 0 {
		if a.balance+amount < 0 {
			return 0, false
		}
		a.balance += amount
		return a.balance, true
	}
	a.balance += amount
	return a.balance, true
}

func (a *Account) Close() (payout int64, ok bool) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	if a.status {
		a.status = false
		return a.balance, true
	}
	return 0, false
}

func (a *Account) Balance() (balance int64, ok bool) {
	a.Mutex.Lock()
	defer a.Mutex.Unlock()
	if a.status {
		return a.balance, true
	}
	return 0, false
}
